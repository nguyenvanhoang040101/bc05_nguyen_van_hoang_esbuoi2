import { dataGlasses } from "./data.js";

const changeGlasses = (id) => {
  let itemInfo = ``;
  dataGlasses.forEach((e) => {
    if (e.id === id) {
      itemInfo += `
      <div class="row pl-2" id="display-attribute-2">
        <p class="title"> ${e.id} - ${e.name} - ${e.brand} ( ${e.color})</p>
         <div class="row">
            <p class="price col-4"> ${e.price}</p>
            <p class="status col-4">Stocking</p>
          </div>
          <p class="desc"> ${e.description}</p>
      </div>
      `;
      document.getElementById(`avatar`).innerHTML = `
          <image id="display-attribute" src="${e.virtualImg}" id="${e.id}"></image>
         `;
    }
  });
  document.getElementById(`glassesInfo`).innerHTML = itemInfo;
};

const renderGlassesList = (dataGlasses) => {
  let content = ``;
  dataGlasses.map((item) => {
    content += `
        <image class="vglasses__items col-4" src="${item.src}" id="${item.id}" onclick="changeGlasses('${item.id}')"></image>
    `;
  });
  document.getElementById(`vglassesList`).innerHTML = content;
};

renderGlassesList(dataGlasses);

const removeGlasses = (index) => {
  if (index) {
    document.getElementById(`display-attribute`).style.display = `flex`;
    document.getElementById(`display-attribute-2`).style.display = `block`;
  } else {
    document.getElementById(`display-attribute`).style.display = `none`;
    document.getElementById(`display-attribute-2`).style.display = `none`;
  }
};

window.removeGlasses = removeGlasses;
window.changeGlasses = changeGlasses;
